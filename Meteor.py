import pygame
import PIL
from PIL import Image

class Meteor(pygame.sprite.Sprite):

	def __init__(self,x,y,enemies,all_sprite_list,mainship):
		pygame.sprite.Sprite.__init__(self)

		self.im = Image.open('Sprites/Meteor5.gif')
		self.cnt = 0
		self.energy = 3
		self.points = 50
		self.image = pygame.image.load('Sprites/Meteor5.gif').convert_alpha()
		self.rect = self.image.get_rect()
		self.rect.bottom = y
		self.rect.centerx = x
		self.all_sprite_list = all_sprite_list
		self.z = 0
		
		self.mainship = mainship
		
		self.killme = False
		self.killmecnt = 10
		
		self.enemies = enemies
		
	def animate(self):
	
		self.cnt += 1
		
		self.rect.y += 5
		self.rect.x += 3
		
		w,h = pygame.display.get_surface().get_size()
		
		if(self.rect.top > h):
			self.kl()
		
		if(self.cnt < 59):
			self.im.seek(self.cnt)
			self.image = pygame.image.fromstring(self.im.convert("RGBA").tobytes(), self.im.size, "RGBA")
		else:
			self.cnt = 0
			self.im.seek(0)
			self.image = pygame.image.fromstring(self.im.convert("RGBA").tobytes(), self.im.size, "RGBA")
			
		if(self.killme):
			if(self.killmecnt>0):
				self.killmecnt -= 1
			else:
				self.kl()
	
	def kl(self):
		self.kill()
		self.enemies.remove(self)
		self.all_sprite_list.update()
			
	def destroyme(self):
		self.im = Image.open('Sprites/Explosion.gif')
		self.cnt = 0
		self.killme = True
		self.mainship.add_points(self.points)
	
			